import numpy as np
import matplotlib.pyplot as plt

x=np.random.randint(1,7,100)
print("1=",np.count_nonzero(x==1), "\n2=",np.count_nonzero(x==2),"\n3=",np.count_nonzero(x==3),"\n4=",np.count_nonzero(x==4),"\n5=",np.count_nonzero(x==5),"\n6=",np.count_nonzero(x==6))
labels=["1","2","3","4","5","6"]


plt.xlabel("Vrijednost na kocki")
plt.ylabel("Koliko puta je kockica pala na taj broj")
plt.title("Bacanje kockice")

plt.hist(x,align="mid",color="red")

#plt.bar(labels,x,color="red")
plt.show()