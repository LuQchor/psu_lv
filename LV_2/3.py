import numpy as np 
import matplotlib.pyplot as plt

data = np.loadtxt(open("LV_2/mtcars.csv", "rb"), usecols=(1, 2,3,4,5,6), delimiter=",", skiprows=1)

plt.xlabel("mpg")
plt.ylabel("hp")
plt.scatter(data[:,0],data[:,3],s=data[:,5]*5)

minmpg=20
maxmpg=20

for x in data[:,0]:
    if (x>maxmpg):
        maxmpg=x
    if (x<minmpg):
        minmpg=x
print("Maxmpg: ",maxmpg,"\nMinmpg: ",minmpg)
plt.show()