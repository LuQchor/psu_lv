import matplotlib.pyplot as plt
import numpy as np
import skimage.io

img = skimage.io.imread('tiger.png', as_gray=True)
red = int(img.size / img[0].size)
stupac = img[0].size

#svjetla slika
max=img.max()
print("Original max=",max)
svjetlaslika=img.copy()
for i in range(red):
    for j in range(stupac):
        svjetlaslika[i][j]+=50
max=svjetlaslika.max()
print("svjetlaslika max=",max)

plt.subplot(131), plt.imshow(svjetlaslika, cmap='gray', vmin=0, vmax=255)
plt.title("Svjetla Slika")
plt.subplot(132), plt.imshow(img, cmap='gray', vmin=0, vmax=255)
plt.title("Originalna Slika")
plt.show()


#rotirana slika za 90%
rotiranaslika = np.zeros((stupac, red))
for i in range(red):
    for j in range(stupac):
        rotiranaslika[j][red -1 -i] = img[i][j]

plt.subplot(131), plt.imshow(rotiranaslika, cmap='gray', vmin=0, vmax=255)
plt.title("Rotirana Slika")
plt.subplot(132), plt.imshow(img, cmap='gray', vmin=0, vmax=255)
plt.title("Originalna Slika")
plt.show()


#zrcaljena slika
zrcaljenaslika=np.zeros((red ,stupac))
for i in range (red):
    for j in range (stupac):
        zrcaljenaslika[i][j]=img[i][stupac-1-j]

plt.subplot(131), plt.imshow(zrcaljenaslika, cmap='gray', vmin=0, vmax=255)
plt.title("Zrcaljena Slika")
plt.subplot(132), plt.imshow(img, cmap='gray', vmin=0, vmax=255)
plt.title("Originalna Slika")
plt.show()


#smanjena rezolucija

smanjenarezolucija=np.zeros(((int(red/8)),(int(stupac/8))))
for i in range(red/8):
    for j in range (stupac/8):
        smanjenarezolucija[i][j]=img[i*8][j*8]

plt.subplot(131), plt.imshow(smanjenarezolucija, cmap='gray', vmin=0, vmax=255)
plt.title("Slika smanjene rezolucije")
plt.subplot(132), plt.imshow(img, cmap='gray', vmin=0, vmax=255)
plt.title("Originalna Slika")
plt.show()            


#cropana slika
novaslika=img.copy()
for i in range(red):
        for j in range(stupac):
            if(j < int(stupac/4) or j > int(stupac/2)):
                novaslika[i][j]=0
            
plt.subplot(131), plt.imshow(novaslika, cmap='gray', vmin=0, vmax=255)
plt.title("Nova slika")
plt.subplot(132), plt.imshow(img, cmap='gray', vmin=0, vmax=255)
plt.title("Originalna Slika")
plt.show()
