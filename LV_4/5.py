import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import sklearn.linear_model as lm

import sklearn.linear_model as lm
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from sklearn.metrics import mean_absolute_error, r2_score, mean_squared_error, max_error


#df = pd.read_csv('cars_processed.csv')
#print(df.info())

#print (df.sort_values(by=['selling_price']).head(1),df.sort_values(by=['selling_price']).tail(1)) 
#print(sum(df['year']==2012))
#print (df.sort_values(by=['km_driven']).head(1),df.sort_values(by=['km_driven']).tail(1))

df = pd.read_csv('cars_processed.csv')
df = df.drop(['name', 'mileage'], axis=1)

X = df[['km_driven', 'year', 'engine', 'max_power']]
Y = df[['selling_price']]

X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2, random_state=10)

Scaler = MinMaxScaler()
X_train_s = Scaler.fit_transform(X_train)
X_test_s = Scaler.transform(X_test)

linear_model = lm.LinearRegression()
linear_model.fit(X_train_s, Y_train)

Y_pred_train = linear_model.predict(X_train_s)
Y_pred_test = linear_model.predict(X_test_s)

print("R2 test: ", r2_score(Y_pred_test, Y_test))
print("RMSE test: ", np.sqrt(mean_squared_error(Y_pred_test, Y_test)))
print("Max error test: ", max_error(Y_pred_test, Y_test))
print("MAE test: ", mean_absolute_error(Y_pred_test, Y_test))