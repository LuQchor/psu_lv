import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars=pd.read_csv('mtcars.csv')

print("5 automobila ima najveću potrošnju\n",mtcars.sort_values("mpg").head(5))

print("Tri automobila s 8 cilindara imaju najmanju potrošnju\n",mtcars[mtcars.cyl==8].sort_values("mpg").tail(3))

print("Srednja potrošnja automobila sa 6 cilindara" ,mtcars[mtcars.cyl==6].mpg.mean())

print("Srednja potrošnja automobila s 4 cilindra mase između 2000 i 2200 lbs" ,mtcars[(mtcars.cyl==4)&(mtcars.wt<2.2)&(mtcars.wt>2.0)].mpg.mean())

print("Automobil s automatskim mjenjačem :",mtcars.value_counts("am")[0] ,"\n Automobil s ručnim mjenjačem", mtcars.value_counts("am")[1])

print("Automobili s rucnim mjenjacem i snagom vecom od 100 hp:",len(mtcars[(mtcars.am==1)&(mtcars.hp>100)]))

print("Masa svakog automobila u kilama\n",mtcars.wt*1000*0.453)