import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars=pd.read_csv('mtcars.csv')

#mtcars.groupby('cyl')['mpg'].mean().plot.bar()
#mtcars.boxplot(column="wt", by="cyl")
#mtcars.groupby('am')['mpg'].mean().plot.bar()

fig = plt.figure()
ax = fig.add_subplot()
ax.scatter(mtcars[mtcars.am == 0].qsec, mtcars[mtcars.am == 0].hp, color="red", label="Auto")
ax.scatter(mtcars[mtcars.am == 1].qsec, mtcars[mtcars.am == 1].hp, color="blue", label="Manual")
ax.legend(loc="upper right")

plt.show()
