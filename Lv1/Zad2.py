try:
    o=float(input("Ocjena "))
except:
    print("Neispravan unos")
    exit()

if (o<0 or o>1):
    print("Ocjena mora biti <0,1>")
    exit()

if (o<0.6):
    print("F")
elif (o<0.7):
    print("D")
elif (o<0.8):
    print("C")
elif (o<0.9):
    print("B")
else:
    print("A")